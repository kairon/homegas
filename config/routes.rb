Rails.application.routes.draw do

  root to: 'pages#index'

  devise_for :admins, controllers: { registrations: "admins/registrations",  sessions: 'admins/sessions' }
  devise_for :users, :controllers => { registrations: "users/registrations", sessions: 'users/sessions' }
  devise_for :sellers, :controllers => { registrations: "sellers/registrations", sessions: 'sellers/sessions' }
  
  get '/busca' => 'pages#search'
  post '/busca' => 'pages#search'
  post '/zipcode' => 'pages#zipcode'
  post '/pedir' => 'pages#create'
  get '/login' => 'pages#login'
  get '/login-vendedor' => 'pages#login_sellers'
  get '/cadastro-vendedor' => 'pages#register_seller'

  get '/painel/pedidos' => 'sellers#orders'
  get '/painel/financeiro' => 'sellers#finances'
  get '/painel/locais' => 'sellers#index'
  get '/painel/dados' => 'sellers#edit'
  get '/painel' => 'sellers#index'
  get '/painel/invoice/:id' => 'sellers#view_invoice'
  get '/painel/entrega/:id' => 'sellers#view_delivery'
  get '/painel/entregar/:id' => 'sellers#deliver'
  get '/painel/entregado/:id' => 'sellers#delivered'
  get '/painel/cancelar/:id' => 'sellers#cancel'
  

  # páginas admin
  get '/admin' => 'admins#index'
  get '/admin/new' => 'admins#new'
  get '/admin/edit' => 'admins#edit'
  get '/admin/admins' => 'admins#admins'
  get '/admin/admins/apagar/:id' => 'admins#destroy_admin'
  get '/admin/clientes' => 'admins#users'
  get '/admin/clientes/:id' => 'admins#view_user'
  get '/admin/clientes/apagar/:id' => 'admins#destroy_user'
  get '/admin/vendedores' => 'admins#sellers'
  get '/admin/vendedores/:id' => 'admins#view_seller'
  get '/admin/pedidos' => 'admins#orders'
  get '/admin/pedidos/apagar/:id' => 'admins#destroy_order'
  get '/pedidos' => 'pages#orders'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
