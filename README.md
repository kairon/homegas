[![Link para o vídeo](http://img.youtube.com/vi/XP8p2YtSnkg/0.jpg)](http://www.youtube.com/watch?v=XP8p2YtSnkg)

# HomeGás

O HomeGás é um serviço para pedir gás de cozinha e entregar em casa. Com ele, é possível que vendedores autorizados se cadastrem para vender nas localidades as quais este deseja atender.

Algumas coisas não foram implementadas. Ex.: validação de campos.

A busca de um vendedor é feita com base na localização de sua revenda. Assim, o cliente que desejar usar o serviço informa seu CEP, este CEP é procurado, usando uma API e retorna o endereço para aquele CEP. O sistema verifica se há vendedores no mesmo bairro, se existirem, o cliente poderá continuar.

# Classes

Diversas classes e diversos métodos foram criados. Explicar cada um é pouco intuitivo, já que a maioria são actions de controllers. O importante é entender a organização da estrutura do banco de dados.

Imagem ilustrativa:

![](http://i.imgur.com/pgkp6af.png)

# Models

Não foi criado nenhum método especial para as models, foram utilizados os já disponíveis no ActiveRecord.

* Admins: administradores, têm acesso a todo o sistema, pode excluir vendedores, excluir pedidos, excluir clientes e outros admins.

* Sellers: Vendedores possuem um painel, através do qual poderão gerenciar os pedidos recebidos por sua loja.

* Users: Clientes podem fazer pedidos no sistema e possuem uma página mostrando o histórico de seus pedidos.

* Orders: Pedidos são criados pelos clientes. A existência de um pedido, depende da existência de um cliente e um vendedor, é uma relação de composição.

# Controllers

* Admins: Utilizada para o painel de controle do site. Apenas administradores podem excluir cliente, vendedores e pedidos (login obrigatório como admin).
 - index: retorna os últimos pedidos realizados;
 - sellers: retorna a lista de vendedores;
 - users: retorna a lista de usuários;
 - admins: retorna os administradores cadastrados;
 - view_user: retorna os pedidos de um cliente e suas informações de cadastro;
 - view_seller: retorna os pedidos de um vendedor e suas informações de cadastro;
 - orders: retorna a lista de pedidos;
 - destroy_order: apaga um pedido pelo id;
 - destroy_seller: apaga um vendedor pelo id;
 - destroy_user: apaga um cliente pelo id;
 - destroy_admin: apaga um admin pelo id (não é possível excluir a si próprio).

* Pages: Utilizada para as páginas principais do site e área de login de um cliente, para acompanhamento de pedidos.
 - search: página de busca de um vendedor (o vendedor é procurado pelo bairro, se não for encontrado, o usuário é redirecionado para a página inicial);
 - zipcode: retorna as informações para um determinado CEP;
 - orders: retorna a lista de pedidos para o cliente (login como usuário obrigatório);
 - create: action para criação de novos pedidos  (login como usuário obrigatório).
 - edit: atualizar os dados do cliente (não implementado);
 
* Sellers: Utilizada para as páginas do painel do vendedor (login como vendedor obritatório).
 - index: retorna os pedidos pendentes e pedidos em fase de entrega;
 - orders: retorna a lista de pedidos recebidos pelo vendedor;
 - view_invoice: página para detalhamento de gastos de um pedido;
 - view_delivery: página como informações utéis para a entrega de um pedido (mapa dinâmico com rota para o destino);
 - deliver: atualiza o status de um pedido para "em entrega";
 - delivered: atualiza o status de um pedido para "entregado";
 - cancel: atualiza o status de um pedido para "cancelado";
 - finances: retorna as finanças (espécie de relatório com números de vendas mensais e faturamento) (não implementado);
 - edit: atualizar os dados do vendedor (não implementado);
 
# Device - Controllers

Além das citadas acima, alguns métodos da device foram sobrecarregados.
* admins/sessions_controller
 - after_sign_in_path_for: redirecionar o admin para o painel após o login;
* admins/registrations_controller
 - sign_up_params: parâmetros permitidos adicionados;
 - account_update_params: parâmetros permitidos adicionados;
 - after_sign_up_path_for: redirecionar o admin para o painel após o cadastro;
* sellers/sessions_controller
 - after_sign_in_path_for: redirecionar o vendedor para o painel após o login;
* sellers/registrations_controller
 - sign_up_params: parâmetros permitidos adicionados;
 - account_update_params: parâmetros permitidos adicionados;
 - after_sign_up_path_for: redirecionar o vendedor para o painel após o cadastro;
* users/registrations_controller
 - sign_up_params: parâmetros permitidos adicionados;
 - account_update_params: parâmetros permitidos adicionados;
 - after_sign_up_path_for: redirecionar o cliente para a página de pedidos, ou a página de busca (definido por um cookie);
* users/sessions_controller
 - after_sign_in_path_for: redirecionar o cliente para a página de pedidos, ou a página de busca (definido por um cookie);
 
# Helpers
Como as controllers são heranças da Application controller, algumas funções foram criadas na application_helper.
* get_status_label: retorna uma label para o pedido, de acordo com o id do status;
* get_payment_label: retorna uma label para o pagamento (dinheiro ou cartão);

# Running

Antes de iniciar, é necessário instalar a gem utilizada para consulta de CEP.

https://github.com/prodis/correios-cep 


Para rodar o programa:

```
rake db:drop db:create db:migrate

rails s
```

# Using

Crie uma conta de vendedor;
Faça um pedido, utilizando um CEP onde o bairro seja o mesmo de algum vendedor;

Para gerenciar tudo, crie uma conta de admin: (opcional)

```
localhost:3000/admins/sign_up
```

# Others

A fim de deixar o site mais dinâmico e prático, foi usado Ajax, da biblioteca jquery e o Google Maps em algumas páginas.


