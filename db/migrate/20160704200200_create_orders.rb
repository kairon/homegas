class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.column :value, :float
      t.column :status, :interger
      t.column :payment, :interger

      t.datetime :delivery_date
      t.text :information

      t.references :user, foreign_key: true
      t.references :seller, foreign_key: true

      t.timestamps null: false
    end
  end
end
