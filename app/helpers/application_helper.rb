module ApplicationHelper
	def header(text)
  		content_for(:header) { text.to_s }
	end

	# Retorna uma label para o status do pedido
	def get_status_label(id)
		case id
        when 0
        	data = '<span class="label label-warning">PENDENTE</span>'
        when 1
        	data = '<span class="label label-success">EM ENTREGA</span>'
        when 2
        	data = '<span class="label label-info">ENTREGUE</span>'
        when 3
        	data = '<span class="label label-danger">CANCELADO</span>'
        else
        	data = '<span class="label label-primary">NO IDENT</span>'
        end
        return data.html_safe
    end

    # Retorna uma label para identificar o tipo de pagamento
    def get_payment_label(id)
		case id
        when 0
        	data = '<span class="label label-success">DINHEIRO</span>'
        when 1
        	data = '<span class="label label-info">CARTÃO</span>'
        else
        	data = '<span class="label label-primary">NO IDENT</span>'
        end
        return data.html_safe
    end

    def get_maps_api
        return "AIzaSyB6M5I3i2bO4lZEjYvYDNr4-92d69jzd2s"
    end

end
