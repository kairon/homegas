class AdminsController < ApplicationController

	before_filter :authenticate_admin!
	layout "admin"

	def index
		@orders = Order.joins(:seller).joins(:user).select("orders.*", "sellers.firm", "users.name as name").limit(50)
		
	end

	def sellers
		@sellers = Seller.all
	end

	def edit

	end

	def users
		@users = User.all
	end

	def admins
		@admins = Admin.all
	end

	def view_user
		@user = User.where(id: params[:id]).take
		@orders = Order.joins(:seller).select("orders.*", "sellers.firm", "sellers.name as seller_name").where(user_id: params[:id])
		cookies[:destroy_order_redirect_to] = request.original_url
		#render json: @orders
	end

	def view_seller
		@seller = Seller.where(id: params[:id]).take
		@orders = Order.joins(:user).select("orders.*", "users.name as custumer", "users.neighborhood").where(seller_id: params[:id])
		#render json: @orders
	end

	def orders
		@orders = Order.joins(:seller).joins(:user).select("orders.*", "sellers.firm", "users.name as name")
		cookies[:destroy_order_redirect_to] = request.original_url
	end

	def destroy_order
  		@order = Order.find(params[:id])
  		@order.destroy
  		redirect_to cookies[:destroy_order_redirect_to]
	end

	def destroy_seller
  		Seller.find(params[:id]).destroy
  		redirect_to '/admin/vendedores'
	end

	def destroy_user
  		User.find(params[:id]).destroy
  		redirect_to '/admin/clientes'
	end

	def destroy_admin
		admin_id = params[:id]

		if admin_id.to_i != current_admin.id
			Admin.find(admin_id).destroy
		end
  			
  		redirect_to '/admin/admins'
  	
	end


	def finances
	
	end
end