class HomeController < ApplicationController
  def index
  end

  def show
  	render :layout => "layout_for_show_only"
	end
end
