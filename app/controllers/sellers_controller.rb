class SellersController < ApplicationController

	before_filter :authenticate_seller!
	layout "sellers"

	def index
		@orders = Order.joins(:user).select("orders.*", "users.name as custumer", "users.neighborhood").where(seller_id: current_seller, status: [0, 1])
		
	end

	def edit
	
	end

	def orders
		@orders = Order.joins(:user).select("orders.*", "users.name as custumer", "users.neighborhood").where(seller_id: current_seller).order(id: :desc)
	end

	def view_invoice
		render layout: false
	end

	def view_delivery
		render layout: false
	end

	def deliver
		@order = Order.where(seller_id: current_seller, status: 0, id: params[:id]).update_all(:status => 1)
		render json: @order
	end

	def delivered
		@order = Order.where(seller_id: current_seller, status: 1, id: params[:id]).update_all(:status => 2)
		redirect_to '/painel'
	end

	def cancel
		@order = Order.where(seller_id: current_seller, status: [0, 1], id: params[:id]).update_all(:status => 3)
		redirect_to '/painel'
	end

	def finances
	
	end
end