class Sellers::RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:seller).permit(:firm, :name, :distributor, :cnpj, :cellphone, :zipcode, :neighborhood, :city, :state, :address, :state, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:seller).permit(:distributor, :cellphone, :email, :password, :password_confirmation)
  end

  def after_sign_up_path_for(resource)
    '/painel'
  end
end