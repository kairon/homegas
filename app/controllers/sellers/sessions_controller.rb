class Sellers::SessionsController < Devise::SessionsController

  private

  def after_sign_in_path_for(resource)
    '/painel'
  end
end