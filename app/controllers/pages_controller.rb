class PagesController < ApplicationController
	
	before_action :authenticate_user!, only: [:create, :orders]

	def search
		require 'correios-cep'
		if params[:zipcode]
			@location = Correios::CEP::AddressFinder.get(params[:zipcode])
			@sellers = Seller.where(neighborhood: @location[:neighborhood], city: @location[:city]).take
			cookies[:cep] = params[:zipcode]
		else
			@location = Correios::CEP::AddressFinder.get(current_user.zipcode)
			@sellers = Seller.where(neighborhood: current_user.neighborhood, city: current_user.city).take
		end

		if !@sellers
  			redirect_to '/'
  			return;
		end
		cookies[:searching] = true
	end

	def zipcode
		require 'correios-cep'
		address = Correios::CEP::AddressFinder.get(params[:zipcode])
		render json: address
	end

	def orders
		require 'correios-cep'
		@orders = Order.joins(:seller).select("orders.*", "sellers.firm", "sellers.name as seller_name").where(user: current_user)
		
	end

	def create
		object = Order.new(:user_id => current_user.id, :value => 62, :status => 0, :payment => params[:payment], :seller_id => params[:seller_id], :information => params[:information]).save
		render json: object
		cookies.delete :searching
	end
end