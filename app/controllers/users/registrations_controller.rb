class Users::RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:user).permit(:name, :cpf, :zipcode, :city, :state, :neighborhood, :address, :email, :password, :password_confirmation)
  
  end

  def account_update_params
    params.require(:user).permit(:zipcode, :city, :state, :neighborhood, :address, :email, :password, :password_confirmation)
  end

  def after_sign_up_path_for(resource)
    if cookies[:searching]
      '/busca'
    else
      '/pedidos'
    end
  end
end