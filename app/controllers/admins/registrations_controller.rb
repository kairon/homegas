class Admins::RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:admin).permit(:name, :email, :password, :password_confirmation)
    
  end

  def account_update_params
    params.require(:admin).permit(:email, :password, :password_confirmation)
  end

  def after_sign_up_path_for(resource)
    '/admin'
  end
end