var field = $('.cnpfj');
field.keydown(function(){
    try {
        field.unmask();
    } catch (e) {}
    console.log('ok');
    if(field.val().length < 11){
        field.mask("999.999.999-99");
    } else {
        field.mask("99.999.999/9999-99");
    }                   
});

$(".zipcode").change(function() {
  $.ajax({
   url: '/zipcode',
   data: {
      zipcode: $(this).val()
   },
   beforeSend: function(xhr) {
   	xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
   },
   error: function() {
   		alert("Não foi possível achar o endereço.")
   },
   dataType: 'json',
   success: function(data) {
   		$('.city_field').val(data.city);
   		$('.state_field').val(data.state);
   		$('.neighborhood_field').val(data.neighborhood);
   		$('.address_field').val(data.address);

   },
   type: 'POST'
});
});

$("#order").click(function(e) {
  e.preventDefault();
  console.log('ok');
  $.ajax({
   url: '/pedir',
   data: {
      seller_id: $('input[name="seller_id"]').val(),
      information: $('textarea[name="information"]').val(),
      payment: $('select[name="payment"]').val()
   },
   beforeSend: function(xhr) {
    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
   },
   error: function(e) {
      console.log('erro');
   },
   dataType: 'json',
   success: function(data) {
      if(data){
        window.location = "/pedidos";
        return;
      }
      alert("Erro ao realizar pedido!");

   },
   type: 'POST'
});
});

$("a[data-toggle='modal']").on('click', function(e){
  e.preventDefault();
  $('#myModal').modal('show').find('.modal-content').load($(this).attr('href'), function() {
    $("#delivery").click(function(e){
      id = $(this).attr('order-id');
      $.ajax({
        url: '/painel/entregar/'+id,
        error: function(e) {
          console.log('erro');
          console.log(e);
        },
        dataType: 'json',
        success: function(data) {
          console.log('sucesso');
          if(data){
            $("#delivered_btn_"+id).removeClass('hidden');
            $('#myModal').modal('hide');
          }
        },
        type: 'GET'
      });
    });
  });
});

$("#map_main").height($( document ).height()-54);




